#! /bin/bash
# @(#) File: daemon-config-parser.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date         Name                       Description
# 10/06/2020   Mannie Iroanya             function to parse config values
#                             
# ---------------------------------------------------------------------------

parse_configuration (){
options=`awk -F '=' '{print $1}' $file`
for option in ${options[@]}
do
value=`awk -F '=' -v o="${option}=" -e '$0 ~ o {print $2}' $file`
declare $option=$value
echo -e "${option}=${value}"
done
}
file=$1
parse_configuration